/**
 * Operating Systems 2013-2017 - Assignment 2
 *
 * Author : Andrei Bogdan Alexandru | 336CB | Tema 2 ~ SO ~ Windows
 *
 */

#undef _UNICODE
#undef UNICODE

#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "cmd.h"
#include "utils.h"

#define READ		0
#define WRITE		1
#define MAX_CMD_SIZE	128

#define PIPE_BUFF_SIZE 16 * 1024 * 1024 // 16MB

/*	Structura ce va fi folosita la crearea unui nou Thread
 * pentru transmiterea parametrilor catre functia executata
 */
typedef struct ParamsStruct {
	command_t *cmd;
	int level;
	command_t *father;
	void *h;
} ParamsStruct;

static void MyCloseHandle(HANDLE h)
{
	int ret;

	ret = CloseHandle(h);
	DIE(ret == 0, "CloseHandle");
}

static void CloseProcess(LPPROCESS_INFORMATION lppi)
{
	MyCloseHandle(lppi->hThread);
	MyCloseHandle(lppi->hProcess);
}

static PCHAR ExpandVariable(PCSTR key)
{
	DWORD dwRes;
	PCHAR varBuf = malloc(MAX_CMD_SIZE);

	DIE(varBuf == NULL, "malloc");

	memset(varBuf, 0, MAX_CMD_SIZE);
	dwRes = GetEnvironmentVariable(key, varBuf, MAX_CMD_SIZE);

	/* Nu am gasit nicio variabila cu acest nume, returnam "" */
	if (!dwRes) {
		free(varBuf);
		varBuf = _strdup("");
		DIE(varBuf == NULL, "_strdup");
	}

	return varBuf;
}

static BOOL SetVariable(PCSTR key, PCSTR value)
{
	if (!SetEnvironmentVariable(key, value)) {
		perror("SetEnvironmentVariable");
		return false;
	}
	return true;
}

static void expand_word(word_t *word)
{
	int ret;
	word_t *part;
	char result[1024] = "";

	if (word == NULL)
		return;

	for (part = word; part != NULL; part = part->next_part) {
		if (part->expand == true) {
			part->string = ExpandVariable(part->string);
			if (part->string == NULL) {
				part->string = _strdup("");
				DIE(part->string == NULL, "_strdup");
			}
		}
		ret = sprintf(result, "%s%s", result, part->string);
		DIE(ret == -1, "sprintf");
	}
	word->string = _strdup(result);
	DIE(word->string == NULL, "_strdup");
}

static void expand_command(simple_command_t *s)
{
	word_t *param;

	expand_word(s->verb);
	for (param = s->params; param != NULL; param = param->next_word)
		expand_word(param);
	expand_word(s->in);
	expand_word(s->out);
	expand_word(s->err);
}

/**
 *	Vom redirecta filedes primit catre un fisier nou in functie de
 * tipul redirectarii (normal sau append) si de filedes (stdin/out/err)
 */
static HANDLE MyOpenFile(PCSTR filename, int type, int io_flag)
{
	SECURITY_ATTRIBUTES sa;

	ZeroMemory(&sa, sizeof(sa));
	sa.bInheritHandle = TRUE;

	switch (type) {
	case STD_INPUT_HANDLE:
		return CreateFile(
			filename,
			GENERIC_READ,
			FILE_SHARE_READ,
			&sa,
			OPEN_ALWAYS,
			FILE_ATTRIBUTE_NORMAL,
			NULL);
	case STD_OUTPUT_HANDLE:
	case STD_ERROR_HANDLE:
		if (io_flag == IO_REGULAR)
			return CreateFile(
				filename,
				GENERIC_WRITE,
				FILE_SHARE_WRITE,
				&sa,
				CREATE_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL);
		else if (io_flag == IO_OUT_APPEND ||
				 io_flag == IO_ERR_APPEND)
			return CreateFile(
				filename,
				GENERIC_WRITE,
				FILE_SHARE_WRITE,
				&sa,
				OPEN_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL);
	default:
		return INVALID_HANDLE_VALUE;
	}
}

/**
 * Verificam ce redirectari avem de facut pentru comanda curenta
 */
static void RedirectHandleFiles(simple_command_t *s, HANDLE *hFiles,
							STARTUPINFO *psi)
{
	DWORD ret;

	psi->hStdInput = GetStdHandle(STD_INPUT_HANDLE);
	DIE(psi->hStdInput == INVALID_HANDLE_VALUE, "GetStdHandle");

	psi->hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	DIE(psi->hStdOutput == INVALID_HANDLE_VALUE, "GetStdHandle");

	psi->hStdError = GetStdHandle(STD_ERROR_HANDLE);
	DIE(psi->hStdError == INVALID_HANDLE_VALUE, "GetStdHandle");

	psi->dwFlags |= STARTF_USESTDHANDLES;

	/* Redirectam stdin*/
	if (s->in != NULL && s->in->string != NULL) {
		hFiles[0] = MyOpenFile(s->in->string,
			STD_INPUT_HANDLE, s->io_flags);
		DIE(hFiles[0] == INVALID_HANDLE_VALUE, "MyOpenFile");
		psi->hStdInput = hFiles[0];
	}

	/*	Daca avem cmd &> file vom seta
	 * stdout si stderr pe acelasi fd
	 */
	if (s->out != NULL && s->err != NULL &&
		strcmp(s->out->string, s->err->string) == 0) {

		hFiles[1] = MyOpenFile(s->out->string,
			STD_OUTPUT_HANDLE, s->io_flags);
		DIE(hFiles[1] == INVALID_HANDLE_VALUE, "MyOpenFile");
		psi->hStdOutput = hFiles[1];

		hFiles[2] = hFiles[1];
		psi->hStdError = hFiles[2];
		return;
	}

	/* Redirectam stdout normal */
	if (s->out != NULL && s->out->string != NULL) {
		hFiles[1] = MyOpenFile(s->out->string,
			STD_OUTPUT_HANDLE, s->io_flags);
		DIE(hFiles[1] == INVALID_HANDLE_VALUE, "MyOpenFile");
		if (s->io_flags == IO_OUT_APPEND) {
			ret = SetFilePointer(hFiles[1], 0, NULL, FILE_END);
			DIE(ret == INVALID_SET_FILE_POINTER, "SetFilePointer");
		}
		psi->hStdOutput = hFiles[1];
	}
	/* Redirectam stderr normal */
	if (s->err != NULL && s->err->string != NULL) {
		hFiles[2] = MyOpenFile(s->err->string,
			STD_ERROR_HANDLE, s->io_flags);
		DIE(hFiles[2] == INVALID_HANDLE_VALUE, "MyOpenFile");
		if (s->io_flags == IO_ERR_APPEND) {
			ret = SetFilePointer(hFiles[2], 0, NULL, FILE_END);
			DIE(ret == INVALID_SET_FILE_POINTER, "SetFilePointer");
		}
		psi->hStdError = hFiles[2];
	}
}

static void CloseFiles(simple_command_t *s, HANDLE *hFiles)
{
	if (s->in != NULL && s->in->string != NULL)
		MyCloseHandle(hFiles[0]);

	if (s->out != NULL && s->out->string != NULL) {
		if (hFiles[1] == hFiles[2]) {
			MyCloseHandle(hFiles[1]);
			return;
		}
		MyCloseHandle(hFiles[1]);
	}

	if (s->err != NULL && s->err->string != NULL)
		MyCloseHandle(hFiles[2]);
}

/**
 * Internal change-directory command.
 */
static bool shell_cd(const simple_command_t *s)
{
	char *filename = NULL;
	HANDLE hFile = NULL;

	if (s->out != NULL && s->out->string != NULL) {
		filename = _strdup(s->out->string);
		DIE(filename == NULL, "_strdup");
	}
	if (s->err != NULL && s->err->string != NULL) {
		filename = _strdup(s->err->string);
		DIE(filename == NULL, "_strdup");
	}

	if (filename != NULL) { /* Redirectam stdout normal */
		hFile = MyOpenFile(filename, STD_OUTPUT_HANDLE, s->io_flags);
		DIE(hFile == INVALID_HANDLE_VALUE, "MyOpenFile");
	}

	if (s->params == NULL)
		return false;

	if (SetCurrentDirectory(s->params->string) == 0) {
		fprintf(stderr, "Unable to find file or directory!\n");
		if (hFile != NULL)
			MyCloseHandle(hFile);
		return false;
	}
	if (hFile != NULL)
		MyCloseHandle(hFile);
	return true;
}

/**
 * Internal exit/quit command.
 */
static int shell_exit(void)
{
	return SHELL_EXIT; /* TODO replace with actual exit code */
}

/**
 * Parse and execute a simple command, by either creating a new processing or
 * internally process it.
 */
static bool parse_simple(simple_command_t *s, int level, command_t *father,
				HANDLE *hRead, HANDLE *hWrite)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	BOOL result;
	DWORD dwRes;
	BOOL bRes;
	HANDLE hFiles[3];
	char command[1024] = "";
	char args[1024] = "";
	word_t *param;

	/* Verificam daca avem true / false */
	if (strncmp("true", s->verb->string, strlen("true")) == 0)
		return true;
	if (strncmp("false", s->verb->string, strlen("false")) == 0)
		return false;

	/* Testam cele 3 comenzi interne */
	if (strncmp("exit", s->verb->string, strlen("exit")) == 0)
		return shell_exit();
	if (strncmp("quit", s->verb->string, strlen("quit")) == 0)
		return shell_exit();
	if (strncmp("cd", s->verb->string, strlen("cd")) == 0)
		return shell_cd(s);


	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	expand_command(s);
	RedirectHandleFiles(s, hFiles, &si);

	if (hRead != NULL)
		si.hStdInput = hRead;
	if (hWrite != NULL)
		si.hStdOutput = hWrite;

	if (s->verb != NULL && s->verb->string != NULL) {
		sprintf(command, "%s", s->verb->string);
		for (param = s->params; param != NULL && param->string != NULL;
				param = param->next_word)
			sprintf(args, "%s '%s'", args, param->string);
		if (strlen(args) > 1)
			sprintf(command, "%s %s", command, args+1);
	}

	bRes = CreateProcess(
			NULL,
			command,
			NULL,
			NULL,
			TRUE,
			0,
			NULL,
			NULL,
			&si,
			&pi);
	if (!bRes) {
		fprintf(stderr, "Execution failed for '%s'\n", s->verb->string);
		fflush(stderr);
	}

	dwRes = WaitForSingleObject(pi.hProcess, INFINITE);
	if (dwRes == WAIT_FAILED)
		return false;
	DIE(dwRes == WAIT_FAILED, "WaitForSingleObject");

	bRes = GetExitCodeProcess(pi.hProcess, &result);
	if (bRes == false)
		return false;
	DIE(!bRes, "GetExitCodeProcess");
	result = !result;

	CloseProcess(&pi);
	CloseFiles(s, hFiles);

	fflush(stdout);


	return result;
}

static bool RunThread(ParamsStruct *params)
{
	return parse_command(params->cmd, params->level,
		params->father, params->h);
}

/**
 * Process two commands in parallel, by creating two children.
 */
static bool do_in_parallel(command_t *cmd1, command_t *cmd2, int level,
		command_t *father)
{
	DWORD IDThread, dwReturn;
	DWORD dwTlsIndex;
	command_t *cmd[2] = {cmd1, cmd2};
	HANDLE hThread[2];
	ParamsStruct *params[2];
	int i;

	/* allocate TLS index */
	dwTlsIndex = TlsAlloc();
	DIE(dwTlsIndex == TLS_OUT_OF_INDEXES, "Eroare la TlsAlloc");

	for (i = 0; i < 2; i++) {
		params[i] = malloc(sizeof(ParamsStruct));
		params[i]->cmd = cmd[i];
		params[i]->level = level;
		params[i]->father = father;
		params[i]->h = NULL;

		hThread[i] = CreateThread(NULL,
			      0,
			      (LPTHREAD_START_ROUTINE) RunThread,
			      params[i],
			      0,
			      &IDThread);
		DIE(hThread[i] == NULL, "CreateThread");
	}

	for (i = 0; i < 2; i++) {
		dwReturn = WaitForSingleObject(hThread[i], INFINITE);
		DIE(dwReturn == WAIT_FAILED, "WaitForSingleObject");
	}

	/* free TLS index */
	dwReturn = TlsFree(dwTlsIndex);
	DIE(dwReturn == FALSE, "TlsFree");

	return true;
}

/**
 * Run commands by creating an anonymous pipe (cmd1 | cmd2)
 */
static bool do_on_pipe(command_t *cmd1, command_t *cmd2, int level,
		command_t *father, HANDLE *hWritePipe)
{
	int ret;
	SECURITY_ATTRIBUTES sa;
	HANDLE hRead, hWrite;

	BOOL bRes;

	ZeroMemory(&sa, sizeof(sa));
	sa.bInheritHandle = TRUE;

	bRes = CreatePipe(&hRead, &hWrite, &sa, 16777216);
	DIE(bRes == FALSE, "CreatePipe");

	/* Avem multiple pipe */
	if (cmd1->cmd1 != NULL && cmd1->cmd2 != NULL)
		do_on_pipe(cmd1->cmd1, cmd1->cmd2, level, father, hWrite);
	else
		bRes = parse_simple(cmd1->scmd, level, father, NULL, hWrite);
	MyCloseHandle(hWrite);

	bRes = parse_simple(cmd2->scmd, level, father, hRead, hWritePipe);
	MyCloseHandle(hRead);

	return bRes;
}

/**
 * Parse and execute a command.
 */
int parse_command(command_t *c, int level, command_t *father, void *h)
{
	PCHAR var, token;
	bool ret1, ret2, ret;

	if (c->op == OP_NONE) {
		/* Verificam daca avem variabila de mediu */
		if (c->scmd->verb != NULL &&
			c->scmd->verb->next_part != NULL &&
			strcmp("=", c->scmd->verb->next_part->string) == 0 &&
			c->scmd->verb->next_part->next_part != NULL) {

			expand_command(c->scmd);
			var = _strdup(c->scmd->verb->string);
			DIE(var == NULL, "_strdup");

			token = strtok(var, "=");

			token = strtok(NULL, "");
			return SetVariable(var, token);
		}
		return parse_simple(c->scmd, level+1, father, NULL, NULL);
	}

	switch (c->op) {
	case OP_SEQUENTIAL:
		ret1 = parse_command(c->cmd1, level+1, father, h);
		ret2 = parse_command(c->cmd2, level+1, father, h);
		ret = ret1 & ret2;
		break;

	case OP_PARALLEL:
		ret = do_in_parallel(c->cmd1, c->cmd2, level+1, father);
		break;

	case OP_CONDITIONAL_NZERO:
		ret = parse_command(c->cmd1, level+1, father, h);
		if (ret == false)
			ret = parse_command(c->cmd2, level+1, father, h);
		break;

	case OP_CONDITIONAL_ZERO:
		ret = parse_command(c->cmd1, level+1, father, h);
		if (ret == true)
			ret = parse_command(c->cmd2, level+1, father, h);
		break;

	case OP_PIPE:
		ret = do_on_pipe(c->cmd1, c->cmd2, level, father, NULL);
		break;

	default:
		return SHELL_EXIT;
	}

	return ret;
}
